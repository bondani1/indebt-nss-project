package application.controllers;

import application.DTO.AuthResponseDto;
import application.DTO.LoginDto;
import application.DTO.RegisterDto;
import application.config.UserDetails;
import application.services.UserDetailsService;
import application.services.UserService;
import application.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
        } catch (BadCredentialsException e) {
            System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            throw new application.exceptions.BadCredentialsException();
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(loginDto.getUsername());
        final String jwt = jwtUtil.generateToken(userDetails);
        final String role = userDetails.getAuthorities().toString();

        return ResponseEntity.ok(new AuthResponseDto(jwt,role));
    }


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@RequestBody RegisterDto registerDTO) {
        userService.register(registerDTO);
        return "ok";
    }

}
