package application.controllers;

import application.models.User;
import application.services.UserService;
import application.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;


    @Autowired
    public UserController(UserService service) {
        this.userService = service;
    }

    @PreAuthorize("hasAuthority('admin')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllUsersAndDebts() {
        return ResponseEntity.ok(userService.getAllUsersAndDebts());
    }


    public User getCurrentUser() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization").split(" ")[1];
        return userService.findByUsername(new JwtUtil().extractUsername(token));
    }


    @GetMapping(value="/test/{name}")
    public ResponseEntity<?> test(@PathVariable String name) {
        return ResponseEntity.ok(userService.findByUsername(name));
    }


    @GetMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUsernamesStartWith(@PathVariable String username) {
        return ResponseEntity.ok(userService.getUsernamesStartWith(username));
    }


    @GetMapping(value = "/contacts")
    public ResponseEntity<?> getContactsForCurrentUser() {
        User user = getCurrentUser();
        return ResponseEntity.ok(userService.getContactsForUser(user));
    }
}
