package application.controllers;

import application.DTO.StoreCommentDto;
import application.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/comments")
public class CommentController {

    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping
    public ResponseEntity<?> storeComment(@RequestBody StoreCommentDto commentDto) {
        commentService.storeComment(commentDto);
        return ResponseEntity.ok("Ok");
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCommentsForDebt(@PathVariable Integer id) {
        return ResponseEntity.ok(commentService.getCommentsDorDebt(id));
    }
}
