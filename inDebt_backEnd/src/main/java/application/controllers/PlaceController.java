package application.controllers;

import application.DTO.StorePlaceDto;
import application.services.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/places")
public class PlaceController {

    private PlaceService placeService;

    @Autowired
    public PlaceController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @PreAuthorize("hasAuthority('admin')")
    @PostMapping
    public ResponseEntity<?> storePlace(@RequestBody StorePlaceDto placeDto) {
        placeService.store(placeDto);
        return ResponseEntity.ok("OK");
    }

    @GetMapping(value = "/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getNamesStartWith(@PathVariable String name) {
        return ResponseEntity.ok(placeService.getNamesStartWith(name));
    }
}
