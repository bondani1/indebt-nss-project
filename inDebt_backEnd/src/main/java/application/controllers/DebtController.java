package application.controllers;

import application.DTO.DebtDto;
import application.DTO.DebtStoreDto;
import application.models.Debt;
import application.models.User;
import application.services.DebtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/debts")
public class DebtController {

    private DebtService debtService;
    private UserController userController;

    @Autowired
    public DebtController(DebtService debtService, UserController userController) {
        this.debtService = debtService;
        this.userController = userController;
    }

    @PreAuthorize("hasAuthority('admin')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Debt> getAll() {
        return debtService.findAll();
    }

    @GetMapping(value = "/current",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllForCurrentUser(@RequestParam String[] filters) {
        User user = userController.getCurrentUser();
        return ResponseEntity.ok(debtService.getAllForUser(user, filters));
    }


    @GetMapping(value = "/current/{reqUser}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllForCurrentUserFromSpec(@PathVariable String reqUser, @RequestParam String[] filters) {
        User curUser = userController.getCurrentUser();
        return ResponseEntity.ok(debtService.getAllForUserFromSpec(curUser, reqUser, filters));
    }


    @PreAuthorize("hasAuthority('admin')")
    @GetMapping(value = "/{username}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllForSpecUser(@PathVariable String username, @RequestParam String[] filters) {
        return ResponseEntity.ok(debtService.getAllForUser(username, filters));
    }


    @PostMapping
    public ResponseEntity<?> storeDebt(@RequestBody DebtStoreDto debtDto) {
        User currentUser = userController.getCurrentUser();
        debtService.store(debtDto, currentUser);
        return ResponseEntity.ok("OK");
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable int id) {
        debtService.delete(id);
        return ResponseEntity.ok("Deleted");
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable Integer id, @RequestBody DebtDto debtDto) {
        debtService.update(id, debtDto);
        return ResponseEntity.ok("Updated");
    }
}
