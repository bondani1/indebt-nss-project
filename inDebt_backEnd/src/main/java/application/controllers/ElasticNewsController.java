package application.controllers;

import application.models.News;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.hazelcast.internal.json.Json;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.elasticsearch.common.xcontent.XContentFactory.*;
import static sun.net.InetAddressCachePolicy.get;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ElasticNewsController {

    @Autowired
    Client client;

    @PostMapping("/create")
    public String create(@RequestBody News news) throws IOException {
        IndexResponse response = client.prepareIndex("in-debt", "news",  news.getId())
                .setSource(jsonBuilder().startObject().field("name", news.getName())
                        .field("authorName", news.getAuthorName())
                        .field("desc", news.getDescription()).endObject()
                ).get();
        System.out.println("response id:"+response.getId());
        return response.getResult().toString();
    }


    @GetMapping("/view/{id}")
    public Map<String, Object> view(@PathVariable final String id) {
        GetResponse getResponse = client.prepareGet("in-debt", "news", id).get();
        return getResponse.getSource();
    }


    @GetMapping("/view/name/{field}")
    public Map<String, Object> searchByName(@PathVariable final String field) {
        Map<String,Object> map = null;
        SearchResponse response = client.prepareSearch("in-debt")
                .setTypes("news")
                .setSearchType(SearchType.QUERY_AND_FETCH).setQuery(QueryBuilders.matchQuery("name",
                        field)).get();
        List<SearchHit> searchHits = Arrays.asList(response.getHits().getHits());
        map = searchHits.get(0).getSource();
        return map;
    }


    @GetMapping("/update/{id}")
    public String update(@PathVariable final String id) throws IOException {
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index("in-debt").type("news").id(id)
                .doc(jsonBuilder().startObject().field("name", "Rajesh").endObject());
        try {
            UpdateResponse updateResponse = client.update(updateRequest).get();
            System.out.println(updateResponse.status());
            return updateResponse.status().toString();
        } catch (InterruptedException | ExecutionException e) { System.out.println(e); }
        return "Exception";
    }


}
