package application.models;

public class News {

    private String id;
    private String authorName;
    private String name;
    private String description;


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getAuthorName() {
        return authorName;
    }


    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }
}
