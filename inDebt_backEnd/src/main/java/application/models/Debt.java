package application.models;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;


@Entity
@Table(name = "debts")
@NamedQueries({
        @NamedQuery(name = "Debt.findForUser", query = "SELECT d FROM Debt d WHERE d.debtor.id = :user or d.loaner.id = :user"),
       // @NamedQuery(name = "Debt.findContactsForUser", query = "select sub from (SELECT d.debtor.username, SUM(d.amount) FROM Debt d WHERE d.loaner.id = :user group by d.debtor.username) sub group by sub.debtor.username")
        @NamedQuery(name = "Debt.findForUserFromSpec", query = "SELECT d FROM Debt d WHERE d.debtor.id = :cur_user and d.loaner.id = :req_user or d.debtor.id = :req_user and d.loaner.id = :cur_user")
})
public class Debt extends AbstractEntity implements Serializable {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int amount;

    private String Description;

    @Column(nullable = false, name = "was_loaned")
    private LocalDate date;

    private LocalDate deadline;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User loaner;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User debtor;

    @OneToMany
    private Collection<Comment> comments;

    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Tag> tags;

    @Column(nullable = false)
    private State state;

    @ManyToOne
    private Place place;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public int getAmount() {
        return amount;
    }


    public void setAmount(int amount) {
        this.amount = amount;
    }


    public String getDescription() {
        return Description;
    }


    public void setDescription(String description) {
        Description = description;
    }


    public LocalDate getDate() {
        return date;
    }


    public void setDate(LocalDate date) {
        this.date = date;
    }


    public LocalDate getDeadline() {
        return deadline;
    }


    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }


    public User getLoaner() {
        return loaner;
    }


    public void setLoaner(User loaner) {
        this.loaner = loaner;
    }


    public User getDebtor() {
        return debtor;
    }


    public void setDebtor(User debtor) {
        this.debtor = debtor;
    }


    public Collection<Comment> getComments() {
        return comments;
    }


    public void setComments(Collection<Comment> comments) {
        this.comments = comments;
    }


    public Collection<Tag> getTags() {
        return tags;
    }


    public void setTags(Collection<Tag> tags) {
        this.tags = tags;
    }


    public State getState() {
        return state;
    }


    public void setState(State state) {
        this.state = state;
    }


    public Place getPlace() {
        return place;
    }


    public void setPlace(Place place) {
        this.place = place;
    }


    public enum State {
        ACTIVE("active"),
        PAID("paid"),
        EXPIRED("expired");

        private final String name;


        State(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
