package application.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;


@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
        @NamedQuery(name = "User.findByUsernameStartWith", query = "SELECT u.username FROM User u WHERE u.username like :username")
})
public class User extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String username;

    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;

    @OneToMany
    private Collection<Debt> loans;

    @OneToMany
    private Collection<Debt> debts;

    @Column(nullable = false)
    private Role role;

    @Column(nullable = false)
    private String password;

    @OneToMany
    private Collection<Comment> comments;

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }


    public LocalDate getBirthDate() {
        return birthDate;
    }


    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }


    public Collection<Debt> getLoans() {
        return loans;
    }


    public void setLoans(Collection<Debt> loans) {
        this.loans = loans;
    }


    public Collection<Debt> getDebts() {
        return debts;
    }


    public void setDebts(Collection<Debt> debts) {
        this.debts = debts;
    }


    public Role getRole() {
        return role;
    }


    public void setRole(Role role) {
        this.role = role;
    }


    public Collection<Comment> getComments() {
        return comments;
    }


    public void setComments(Collection<Comment> comments) {
        this.comments = comments;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public enum Role {
        USER("user"),
        ADMIN("admin");

        private final String name;


        Role(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
