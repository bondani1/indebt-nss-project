package application.models;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "tags")
@NamedQueries({
        @NamedQuery(name = "Tag.findByName", query = "SELECT t FROM Tag t WHERE t.name = :name"),
        @NamedQuery(name = "Tag.findByNameStartsWith", query = "SELECT t.name FROM Tag t WHERE t.name like :name")
})
public class Tag extends AbstractEntity implements Serializable {

    @Column(nullable = false)
    private String name;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }
}
