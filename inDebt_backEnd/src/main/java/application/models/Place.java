package application.models;

import javax.persistence.*;


@Entity
@Table(name = "places")
@NamedQueries({
        @NamedQuery(name = "Place.findByName", query = "SELECT p FROM Place p WHERE p.name = :name"),
        @NamedQuery(name = "Place.findByNameStartsWith", query = "SELECT p.name FROM Place p WHERE p.name like :name")
})
public class Place extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String city;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getDesc() {
        return description;
    }


    public void setDesc(String desc) {
        this.description = desc;
    }


    public String getCity() {
        return city;
    }


    public void setCity(String city) {
        this.city = city;
    }
}
