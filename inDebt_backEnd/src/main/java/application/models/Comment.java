package application.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "comments")
@NamedQueries({
        @NamedQuery(name = "Comment.findForDebt", query = "SELECT c FROM Comment c WHERE c.debt.id = :debtId")
})
public class Comment extends AbstractEntity {

    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Debt debt;


    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }


    public LocalDateTime getDate() {
        return date;
    }


    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    public User getUser() {
        return user;
    }


    public void setUser(User user) {
        this.user = user;
    }


    public Debt getDebt() {
        return debt;
    }


    public void setDebt(Debt debt) {
        this.debt = debt;
    }
}
