package application.DAO;

import application.models.Comment;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CommentDao extends BaseDao<Comment> {

    protected CommentDao() {
        super(Comment.class);
    }

    public List<Comment> findForDebt(Integer id) {
        return em.createNamedQuery("Comment.findForDebt", Comment.class).setParameter("debtId", id).getResultList();
    }
}
