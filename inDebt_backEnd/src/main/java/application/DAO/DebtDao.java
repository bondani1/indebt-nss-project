package application.DAO;

import application.models.Debt;
import application.models.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;


@Repository
public class DebtDao extends BaseDao<Debt> {

    protected DebtDao() {
        super(Debt.class);
    }


    public List<Debt> findForUser(Integer userId) {
        try {
            return em.createNamedQuery("Debt.findForUser", Debt.class).setParameter("user", userId).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }


    public List findContactsForUser(User user) {
        return em.createNativeQuery("Select  username, Sum(sum) from (SELECT d.debtor_id, SUM(d.amount) FROM debts as d WHERE d.loaner_id = ?1 group by d.debtor_id UNION SELECT d.loaner_id, -SUM(d.amount) FROM debts as d WHERE d.debtor_id = ?1 group by d.loaner_id) x JOIN users on users.id=x.debtor_id group by username").setParameter(1, user.getId()).getResultList();
        //return em.createNamedQuery("Debt.findContactsForUser", String.class).setParameter("user", user.getId()).getResultList();
    }


    public List<Debt> findForUserFromSpec(User curUser, User reqUser) {
        return em.createNamedQuery("Debt.findForUserFromSpec", Debt.class).setParameter("cur_user", curUser.getId()).setParameter("req_user", reqUser.getId()).getResultList();

    }


    public List findAllUsersAndDebts() {
        return em.createNativeQuery("Select  username, Sum(sum) from (SELECT d.debtor_id, -SUM(d.amount) as sum FROM debts as d group by d.debtor_id UNION SELECT d.loaner_id, SUM(d.amount) FROM debts as d group by d.loaner_id) x JOIN users on users.id=x.debtor_id group by username").getResultList();
    }
}
