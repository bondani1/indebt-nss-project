package application.DAO;

import application.models.Place;
import application.models.Tag;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;


@Repository
public class TagDao extends BaseDao<Tag> {

    protected TagDao() {
        super(Tag.class);
    }


    public Tag findByName(String name) {
        try {
            return em.createNamedQuery("Tag.findByName", Tag.class).setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    public List<String> findNamesStartWith(String name) {
        return em.createNamedQuery("Tag.findByNameStartsWith", String.class).setParameter("name", name + "%").getResultList();
    }
}
