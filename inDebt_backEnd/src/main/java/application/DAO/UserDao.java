package application.DAO;

import application.models.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;


@Repository
public class UserDao extends BaseDao<User> {

    public UserDao() {
        super(User.class);
    }


    public User findByUsername(String username) {
        try {
            return em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    public List<String> findUsernamesStartWith(String username) {
        return em.createNamedQuery("User.findByUsernameStartWith", String.class).setParameter("username", username + "%").getResultList();
    }
}
