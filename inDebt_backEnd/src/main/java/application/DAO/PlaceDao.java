package application.DAO;

import application.models.Place;
import application.models.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;


@Repository
public class PlaceDao extends BaseDao<Place> {

    protected PlaceDao() {
        super(Place.class);
    }

    public Place findByName(String name) {
        try {
            return em.createNamedQuery("Place.findByName", Place.class).setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    public List<String> findNamesStartWith(String name) {
        return em.createNamedQuery("Place.findByNameStartsWith", String.class).setParameter("name", name + "%").getResultList();
    }
}
