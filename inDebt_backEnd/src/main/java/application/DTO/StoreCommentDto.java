package application.DTO;

import java.time.LocalDateTime;


public class StoreCommentDto {

    private String username;
    private Integer debtId;
    private String text;


    public String getUsername() {
        return username;
    }


    public void setUsername(String user) {
        this.username = user;
    }


    public Integer getDebtId() {
        return debtId;
    }


    public void setDebt(Integer debt) {
        this.debtId = debt;
    }


    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }


}
