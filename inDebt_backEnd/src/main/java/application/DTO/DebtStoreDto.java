package application.DTO;

import java.time.LocalDate;


public class DebtStoreDto {

    private String name;
    private String descriptopn;
    private LocalDate deadLine;
    private LocalDate date;
    private String participant;
    private int amount;
    private boolean debt;
    private String placeName;
    private String[] tags;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getDescriptopn() {
        return descriptopn;
    }


    public void setDescriptopn(String descriptopn) {
        this.descriptopn = descriptopn;
    }


    public LocalDate getDeadLine() {
        return deadLine;
    }


    public void setDeadLine(LocalDate deadLine) {
        this.deadLine = deadLine;
    }


    public LocalDate getDate() {
        return date;
    }


    public void setDate(LocalDate date) {
        this.date = date;
    }


    public String getParticipant() {
        return participant;
    }


    public void setParticipant(String participant) {
        this.participant = participant;
    }


    public int getAmount() {
        return amount;
    }


    public void setAmount(int amount) {
        this.amount = amount;
    }


    public boolean isDebt() {
        return debt;
    }


    public void setAdebt(boolean debt) {
        this.debt = debt;
    }


    public String getPlaceName() {
        return placeName;
    }


    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }


    public String[] getTags() {
        return tags;
    }


    public void setTags(String[] tags) {
        this.tags = tags;
    }
}
