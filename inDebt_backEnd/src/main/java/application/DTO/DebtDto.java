package application.DTO;

import application.models.Place;
import application.models.Tag;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;

public class DebtDto implements Serializable {

    private int id;
    private String name;
    private String desc;
    private int amount;
    private LocalDate date;
    private LocalDate deadLine;
    private UserDto loaner;
    private UserDto debtor;
    private Collection<Tag> tags;
    private String state;
    private Place place;



    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getDesc() {
        return desc;
    }


    public void setDesc(String desc) {
        this.desc = desc;
    }


    public int getAmount() {
        return amount;
    }


    public void setAmount(int amount) {
        this.amount = amount;
    }


    public LocalDate getDate() {
        return date;
    }


    public void setDate(LocalDate date) {
        this.date = date;
    }


    public LocalDate getDeadLine() {
        return deadLine;
    }


    public void setDeadLine(LocalDate deadLine) {
        this.deadLine = deadLine;
    }


    public UserDto getLoaner() {
        return loaner;
    }


    public void setLoaner(UserDto loaner) {
        this.loaner = loaner;
    }


    public UserDto getDebtor() {
        return debtor;
    }


    public void setDebtor(UserDto debtor) {
        this.debtor = debtor;
    }


    public Collection<Tag> getTags() {
        return tags;
    }


    public void setTags(Collection<Tag> tags) {
        this.tags = tags;
    }


    public String getState() {
        return state;
    }


    public void setState(String state) {
        this.state = state;
    }


    public Place getPlace() {
        return place;
    }


    public void setPlace(Place place) {
        this.place = place;
    }
}
