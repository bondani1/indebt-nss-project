package application.DTO;

import java.time.LocalDateTime;


public class CommentDto {

    private Integer id;
    private String text;
    private LocalDateTime date;
    private UserDto user;
    private DebtDto debt;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }


    public LocalDateTime getDate() {
        return date;
    }


    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    public UserDto getUser() {
        return user;
    }


    public void setUser(UserDto user) {
        this.user = user;
    }


    public DebtDto getDebt() {
        return debt;
    }


    public void setDebt(DebtDto debt) {
        this.debt = debt;
    }
}
