package application.services;

import application.DAO.BaseDao;
import application.DAO.PlaceDao;
import application.DTO.StorePlaceDto;
import application.exceptions.EntityNotFoundException;
import application.models.Place;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class PlaceService extends BaseService<Place>{

    @Autowired
    private PlaceDao placeDao;

    public Place findByName(String name) {
        return (name!=null) ? findByNameOrThrow(name) : null;
    }


    public Place findByNameOrThrow(String name) throws EntityNotFoundException {
        return Optional.ofNullable(placeDao.findByName(name)).orElseThrow(
                () -> new EntityNotFoundException(Place.class, "name", name)
        );
    }

    @Transactional
    public void store(StorePlaceDto placeDto) {
        Place place = new Place();
        place.setName(placeDto.getName());
        place.setCity(placeDto.getCity());
        place.setDesc(placeDto.getDescription());

        placeDao.persist(place);
    }


    public List<String> getNamesStartWith(String name) {
        return placeDao.findNamesStartWith(name);
    }


    @Override
    protected BaseDao<Place> getPrimaryDao() {
        return placeDao;
    }
}
