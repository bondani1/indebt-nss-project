package application.services;

import application.DAO.BaseDao;
import application.DAO.CommentDao;
import application.DTO.CommentDto;
import application.DTO.StoreCommentDto;
import application.models.Comment;
import application.models.Debt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class CommentService extends BaseService<Comment>{

    @Autowired
    private CommentDao commentDao;
    @Autowired
    private UserService userService;
    @Autowired
    private DebtService debtService;


    @Override
    protected BaseDao<Comment> getPrimaryDao() {
        return commentDao;
    }

    @Transactional
    public void storeComment(StoreCommentDto dto) {
        Comment comment = new Comment();
        comment.setUser(userService.findByUsername(dto.getUsername()));
        comment.setDebt(debtService.findOrThrow(dto.getDebtId()));
        comment.setText(dto.getText());
        comment.setDate(LocalDateTime.now());

        persist(comment);
    }

    @Transactional
    public List<CommentDto> getCommentsDorDebt(Integer id) {
        return commentDao.findForDebt(id).stream().map(this::get).collect(Collectors.toList());
    }


    public CommentDto get(Comment comment) {
        CommentDto dto = new CommentDto();
        dto.setId(comment.getId());
        dto.setDate(comment.getDate());
        dto.setDebt(debtService.get(comment.getDebt()));
        dto.setUser(userService.get(comment.getUser()));
        dto.setText(comment.getText());

        return dto;
    }
}
