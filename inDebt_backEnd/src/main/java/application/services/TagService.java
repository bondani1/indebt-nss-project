package application.services;

import application.DAO.BaseDao;
import application.DAO.TagDao;
import application.exceptions.EntityNotFoundException;
import application.models.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;


@Service
public class TagService extends BaseService<Tag> {

    @Autowired
    private TagDao tagDao;


    @Override
    protected BaseDao<Tag> getPrimaryDao() {
        return tagDao;
    }


    public Tag findByName(String name) {
        return findByNameOrThrow(name);
    }


    public Tag findByNameOrThrow(String name) throws EntityNotFoundException {
        return Optional.ofNullable(tagDao.findByName(name)).orElseThrow(
                () -> new EntityNotFoundException(Tag.class, "name", name)
        );
    }


    public List<String> getNamesStartWith(String name) {
        return tagDao.findNamesStartWith(name);
    }


    public Tag findOrCreate(String name) {
        Tag tag = tagDao.findByName(name);
        return (tag == null) ? store(name) : tag;
    }


    public Tag store(String name) {
        Tag tag = new Tag();
        tag.setName(name);

        tagDao.persist(tag);
        return tag;
    }


    public List<Tag> findList(String[] tagNames) {
        return (tagNames != null) ? Arrays.stream(tagNames).map(this::findOrCreate).collect(Collectors.toList()) : null;
    }
}
