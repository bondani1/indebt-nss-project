package application.services;

import application.DAO.BaseDao;
import application.DAO.UserDao;
import application.DTO.RegisterDto;
import application.DTO.UserDto;
import application.exceptions.EntityNotFoundException;
import application.models.Debt;
import application.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class UserService extends BaseService<User> {

    @Autowired
    private UserDao userDao;
    @Autowired
    private DebtService debtService;


    public User findOrThrow(Integer id) throws EntityNotFoundException {
        return Optional.ofNullable(getPrimaryDao().find(id)).orElseThrow(
                () -> new EntityNotFoundException(User.class, "id", id)
        );
    }


    @Transactional
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }


    @Override
    protected BaseDao<User> getPrimaryDao() {
        return userDao;
    }


    @Transactional
    public List<User> findAll() {
        return userDao.findAll();
    }


    public UserDto get(User user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setUsername(user.getUsername());
        dto.setName(user.getName());
        dto.setLastName(user.getLastName());
        dto.setBirthDate(user.getBirthDate());
        return dto;
    }


    @Transactional
    public void register(RegisterDto registerDTO) {
        User user = new User();
        user.setName(registerDTO.getName());
        user.setLastName(registerDTO.getLastName());
        user.setUsername(registerDTO.getUserName());
        user.setBirthDate(registerDTO.getBirthDate());
        user.setPassword(new BCryptPasswordEncoder().encode(registerDTO.getPassword()));
        user.setBirthDate(registerDTO.getBirthDate());
        user.setRole(User.Role.USER);
        userDao.persist(user);
    }


    public List<String> getUsernamesStartWith(String username) {
        return userDao.findUsernamesStartWith(username);
    }


    public List<String> getContactsForUser(User user) {
        return debtService.getContactsForUser(user);
    }


    public List<String> getAllUsersAndDebts() {
        return debtService.getAllUsersAndDebts();
    }
}
