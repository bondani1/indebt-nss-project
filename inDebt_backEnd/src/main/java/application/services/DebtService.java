package application.services;

import application.DAO.BaseDao;
import application.DAO.DebtDao;
import application.DTO.DebtDto;
import application.DTO.DebtStoreDto;
import application.exceptions.EntityNotFoundException;
import application.models.Debt;
import application.models.User;
import com.hazelcast.core.Hazelcast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@CacheConfig(cacheNames = "debtsCache")
public class DebtService extends BaseService<Debt> {

    private DebtDao debtDao;
    private UserService userService;
    private TagService tagService;
    private PlaceService placeService;


    @Autowired
    public DebtService(DebtDao debtDao, UserService userService, TagService tagService, PlaceService placeService) {
        this.debtDao = debtDao;
        this.userService = userService;
        this.tagService = tagService;
        this.placeService = placeService;
    }


    public Debt findOrThrow(Integer id) throws EntityNotFoundException {
        return Optional.ofNullable(getPrimaryDao().find(id)).orElseThrow(
                () -> new EntityNotFoundException(Debt.class, "id", id)
        );
    }


    @Transactional
    public void store(DebtStoreDto debtDto, User user) {
        Debt debt = new Debt();
        debt.setName(debtDto.getName());
        debt.setAmount(debtDto.getAmount());
        debt.setDescription(debtDto.getDescriptopn());
        debt.setDate(debtDto.getDate());
        debt.setDeadline(debtDto.getDeadLine());
        debt.setDebtor((debtDto.isDebt()) ? user : userService.findByUsername(debtDto.getParticipant()));
        debt.setLoaner((debtDto.isDebt()) ? userService.findByUsername(debtDto.getParticipant()) : user);
        debt.setTags(tagService.findList(debtDto.getTags()));
        debt.setPlace(placeService.findByName(debtDto.getPlaceName()));
        debt.setState(Debt.State.ACTIVE);
        debtDao.persist(debt);
    }


    @Transactional
    public List<DebtDto> getAllForUser(User user, String[] adds) {
        List<DebtDto> debts;
        if (adds[0].equals("debts")) {
            debts = findAllForUser(user.getId()).stream()
                    .filter(d -> d.getDebtor().getId().equals(user.getId()))
                    .map(this::get)
                    .collect(Collectors.toList());
        } else if (adds[0].equals("loans")) {
            debts = findAllForUser(user.getId()).stream()
                    .filter(d -> d.getLoaner().getId().equals(user.getId()))
                    .map(this::get)
                    .collect(Collectors.toList());
        } else {
            debts = findAllForUser(user.getId()).stream()
                    .map(this::get)
                    .collect(Collectors.toList());
        }

        if (adds[1].equals("DESC")) {
            debts = debts.stream()
                    .sorted(Comparator.comparing(DebtDto::getDate))
                    .collect(Collectors.toList());
        } else {
            debts = debts.stream()
                    .sorted(Comparator.comparing(DebtDto::getDate).reversed())
                    .collect(Collectors.toList());
        }

        return debts;
    }


    public List<DebtDto> getAllForUserFromSpec(User curUser, String reqUserName, String[] adds) {
        User reqUser = userService.findByUsername(reqUserName);
        return getAllForUserFromSpec(curUser, reqUser, adds);
    }


    @Transactional
    public List<DebtDto> getAllForUserFromSpec(User curUser, User reqUser, String[] adds) {
        List<DebtDto> debts;
        if (adds[0].equals("debts")) {
            debts = findAllForUserFromSpec(curUser, reqUser).stream()
                    .filter(d -> d.getDebtor().getId().equals(curUser.getId()))
                    .map(this::get)
                    .collect(Collectors.toList());
        } else if (adds[0].equals("loans")) {
            debts = findAllForUserFromSpec(curUser, reqUser).stream()
                    .filter(d -> d.getLoaner().getId().equals(curUser.getId()))
                    .map(this::get)
                    .collect(Collectors.toList());
        } else {
            debts = findAllForUserFromSpec(curUser, reqUser).stream()
                    .map(this::get)
                    .collect(Collectors.toList());
        }

        if (adds[1].equals("DESC")) {
            debts = debts.stream()
                    .sorted(Comparator.comparing(DebtDto::getDate))
                    .collect(Collectors.toList());
        } else {
            debts = debts.stream()
                    .sorted(Comparator.comparing(DebtDto::getDate).reversed())
                    .collect(Collectors.toList());
        }

        return debts;
    }


    public DebtDto get(Debt debt) {
        DebtDto dto = new DebtDto();
        dto.setId(debt.getId());
        dto.setAmount(debt.getAmount());
        dto.setName(debt.getName());
        dto.setDesc(debt.getDescription());
        dto.setLoaner(userService.get(debt.getLoaner()));
        dto.setDebtor(userService.get(debt.getDebtor()));
        dto.setDate(debt.getDate());
        dto.setDeadLine(debt.getDeadline());
        dto.setTags(debt.getTags());
        dto.setPlace(debt.getPlace());
        return dto;
    }

    @Cacheable
    public List<Debt> findAllForUser(Integer userId) {
        Map<Integer, List<Debt>> cache = Hazelcast.getHazelcastInstanceByName("hc-instance").getMap("debtsCache");
        List<Debt> debts = cache.get(userId);
        if (debts == null) {
            debts = debtDao.findForUser(userId);
            cache.put(userId,debts);
        }
        return debts;
    }


    public List<Debt> findAllForUserFromSpec(User curUser, User reqUser) {
        return debtDao.findForUserFromSpec(curUser, reqUser);
    }


    @Override
    protected BaseDao<Debt> getPrimaryDao() {
        return debtDao;
    }


    @Transactional
    public void delete(Integer id) {
        Debt debt = findOrThrow(id);
        debtDao.remove(debt);
    }


    @Transactional
    public void update(Integer id, DebtDto debtDto) {
        Debt debt = findOrThrow(id);
        debt.setDescription(debtDto.getDesc());
        debt.setAmount(debtDto.getAmount());

        update(debt);
    }


    public List<String> getContactsForUser(User user) {
        return debtDao.findContactsForUser(user);
    }


    public List<String> getAllUsersAndDebts() {
        return debtDao.findAllUsersAndDebts();
    }


    public List<DebtDto> getAllForUser(String username, String[] adds) {
        User user = userService.findByUsername(username);
        return getAllForUser(user, adds);
    }



}
