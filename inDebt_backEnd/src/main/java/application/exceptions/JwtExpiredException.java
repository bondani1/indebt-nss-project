package application.exceptions;

public class JwtExpiredException extends RuntimeException {

    public JwtExpiredException() {
        super("Session time expired");
    }
}
