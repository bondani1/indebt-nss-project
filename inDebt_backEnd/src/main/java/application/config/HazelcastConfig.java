package application.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class HazelcastConfig {
    @Bean
    public Config hazelCastConfig() {
        return new Config().setInstanceName("hc-instance")
                .addMapConfig(new MapConfig().setName("debtsCache")
                .setMaxSizeConfig(new MaxSizeConfig(200, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
                        .setEvictionPolicy(EvictionPolicy.NONE).setTimeToLiveSeconds(1000));
    }

}
