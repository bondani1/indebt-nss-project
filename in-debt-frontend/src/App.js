import React from 'react';
import './components/styles/App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Login from './components/Login';
import Registration from './components/Registration';
import AdminPage from './components/AdminPage';
import MainStatic from './components/MainStatic';
import NewDebt from './components/NewDebt';
import Users from './components/Users';

function App() {

    return (
        <Router>
            <Switch>
                <Route path="/" exact component={Registration}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/admin" exact component={AdminPage}/>
                <Route path="/static" exact component={MainStatic}/>
                <Route path="/new" exact component={NewDebt}/>
                <Route path="/users" exact component={Users}/>
            </Switch>
        </Router>
    );
}

export default App;
