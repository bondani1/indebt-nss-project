import React, {Component} from 'react';
import './styles/App.css';
import {Link} from 'react-router-dom';


//later remake ids to refs and maybe switch to class components

let token = '';

class Login extends Component {


    constructor() {
        super();
        this.state = {
            authorized: false
        }
        this.usernameLabel = React.createRef();
        this.usernameInput = React.createRef();
        this.passwordLabel = React.createRef();
        this.passwordInput = React.createRef();


    }

    login = () => {
        const username = document.getElementById('loginUsername').value;
        const password = document.getElementById('loginPassword').value;


        const loginDto = {
            username: username,
            password: password
        }


        fetch('/inDebt/auth', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(loginDto)
        })
            .then((response) => response.json())
            .then((data) => {
                token = data.jwt;
                console.log(token);
                localStorage.setItem('token', JSON.stringify(token));
                localStorage.setItem('username', JSON.stringify(loginDto.username));
                localStorage.setItem("role", data.role.substring(1, data.role.length-1))
                this.setState({
                    authorized: true
                })
            });

    }

    moveUsernameLabel = () => {
        this.usernameLabel.current.style.transform = "translate(0px, 0px)";
        this.usernameLabel.current.style.fontSize = "1em";
    }
    moveUsernameLabelBack = () => {
        if (this.usernameInput.current.value == "") {
            this.usernameLabel.current.style.transform = "translate(0px,90%)";
            this.usernameLabel.current.style.fontSize = "1.2em";
        }

    }
    movePasswordLabel = () => {
        this.passwordLabel.current.style.transform = "translate(0px, 0px)";
        this.passwordLabel.current.style.fontSize = "1em";
    }
    movePasswordLabelBack = () => {
        if (this.passwordInput.current.value == "") {
            this.passwordLabel.current.style.transform = "translate(0px,90%)";
            this.passwordLabel.current.style.fontSize = "1.2em";
        }

    }

    render() {
        return (
            <div>
                <label htmlFor="loginUsername" className="loginLabel" id="loginLabelUsername" ref={this.usernameLabel}>
                    Username
                </label>
                <input type="text" id="loginUsername" className="loginInput" ref={this.usernameInput}
                       onFocus={this.moveUsernameLabel} onBlur={this.moveUsernameLabelBack}/>
                <label id="login" htmlFor="loginPassword" className="loginLabel" id="loginLabelPassword"
                       ref={this.passwordLabel}>
                    Password
                </label>
                <input type="password" id="loginPassword" className="loginInput" ref={this.passwordInput}
                       onFocus={this.movePasswordLabel} onBlur={this.movePasswordLabelBack}/>
                <Link to = {this.state.authorized === true ? '/static' : '/login'}>
                    <div id="loginButton" onClick={this.login}>Login</div>
                </Link>
            </div>
        );
    }
}

export default Login;
//
// const login = () => {
//     const username = document.getElementById('defaultFormLoginEmailEx').value;
//     const password = document.getElementById('defaultFormLoginPasswordEx').value;
//
//
//     const loginDto = {
//         username: username,
//         password: password
//     }
//
//
//     fetch('/inDebt/auth', {
//         method: 'POST',
//         headers: { 'Content-Type': 'application/json'},
//         body: JSON.stringify(loginDto)
//       })
//       .then((response) => response.json())
//       .then((data) => {
//         token = data.jwt;
//         console.log(token);
//         localStorage.setItem('token', JSON.stringify(token));
//       });
//
// }
//
//
// const Login = () => {
//
//     return(
//
//         <div>
//             <label htmlFor="defaultFormLoginEmailEx" className="loginLable" id="loginLableTop">
//                 Username
//             </label>
//             <input type="text" id="defaultFormLoginEmailEx" className="loginInput" />
//             <label htmlFor="defaultFormLoginPasswordEx" className="loginLable" >
//                 Password
//             </label>
//             <input type="password" id="defaultFormLoginPasswordEx" className="loginInput"/>
//             <Link  to="/static">
//                 <div  id="loginButton" onClick={login}>Login</div>
//             </Link>
//         </div>
//         // <MDBContainer>
//         //     <MDBRow id = "login-form">
//         //         <MDBCol md="6">
//         //         <form>
//         //             <p className="h4 text-center mb-4">Sign in</p>
//         //             <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
//         //             Your username
//         //             </label>
//         //             <input type="text" id="defaultFormLoginEmailEx" className="form-control" />
//         //             <br/>
//         //             <label htmlFor="defaultFormLoginPasswordEx" className="grey-text">
//         //                 Your password
//         //             </label>
//         //             <input type="password" id="defaultFormLoginPasswordEx" className="form-control"/>
//         //             <div className="text-center mt-4">
//         //                 <Link to="/static">
//         //                     <MDBBtn color="indigo" type="button" onClick={login}>Login</MDBBtn>
//         //                 </Link>
//         //             </div>
//         //         </form>
//         //         </MDBCol>
//         //     </MDBRow>
//         // </MDBContainer>
//     )
//     const username = document.getElementById('defaultFormLoginEmailEx');
//     username.addEventListener('click', (event) => {
//         this.style.backgroundColor = 'pink';
//         console.log("s");
//     });
// }
// const input = document.getElementById('defaultFormLoginEmailEx');
//
//
//
//
//
