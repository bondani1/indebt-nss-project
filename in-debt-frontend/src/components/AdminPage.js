import React from 'react';
import './styles/App.css';


//later remake ids to refs and maybe switch to class components

let token = '';

const listUsers = () => {
    
    token = JSON.parse(localStorage.getItem('token'));

    fetch('/inDebt/users', {
        method: 'GET',
        headers: { 
            'Authorization': `Bearer ${token}`
        }})
        .then((response) => response.json())
        .then((data) => {
            console.log(data);

            //TO DO: make universal condition
            if(data.status !== 403) {
                createListOfUsers(data);
            }
        });
}


const createListOfUsers = (users) => {
    let newDiv = document.createElement('div');
    newDiv.innerHTML = 'Users:';
    let counter = 1;
    for(let user of users) {
        let newP = document.createElement('p');
        newP.innerText = `${counter}) ${user.name}`;
        newDiv.appendChild(newP);
        counter++;
    }
    document.getElementById('container').appendChild(newDiv);
}


const Login = () => {
    return(
        <div id = "container">   
            <button 
                onClick = {listUsers}
                type = "button" 
                style = {
                    {
                        width:100,
                        height:50,
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                    }
                }
            >
                List users
            </button>
        </div>
    )
}

export default Login;