import React from "react";
import HeaderC from './HeaderC';
import BottomBar from "./BottomBar";
import User from "./User";
import Debt from "./Debt";

class Users extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            usersAndDebts: {},
            debtsAndLoans: {},
            showUsers: true,
            contactName: ''
        };
        this.triggerUserClick = this.triggerUserClick.bind(this);
    }

    createUsersAndDebts = (data) => {
      if (data.status !== 403) {

        let uAd = this.state.usersAndDebts;
        for (const elem of data) {
          uAd[elem[0]] = elem[1]
        }

        this.setState({
            usersAndDebts: uAd
        })

        console.log(Object.entries(uAd))
      }
    }


    componentDidMount() {
      let promise;
      let token = JSON.parse(localStorage.getItem('token'));

      if (localStorage.getItem("role")=="admin") {
        promise = fetch(`/inDebt/users`, {
          method: 'GET',
          headers: {
              'Authorization': `Bearer ${token}`
          }
      })
    } else {
      promise = fetch('/inDebt/users/contacts', {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    })
  }
      promise.then((response) => response.json())
      .then((data) => this.createUsersAndDebts(data));
    }


    triggerUserClick = (name) => {
      console.log(name);
      this.setState({
        showUsers: false
      });

      let token = JSON.parse(localStorage.getItem('token'));
      let username = JSON.parse(localStorage.getItem('username'));

      let promise;
      if (localStorage.getItem("role")=="admin") {
        console.log("admin fetch")
          promise = fetch(`/inDebt/debts/${name}?filters=all,DESC`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
      } else {
      promise = fetch(`/inDebt/debts/current/${name}?filters=all,DESC`, {
          method: 'GET',
          headers: {
              'Authorization': `Bearer ${token}`
          }
      })
    }
      promise.then((response) => response.json())
      .then(data => this.groupDebts(data, name))
      console.log(this.state.debtsAndLoans)

      this.setState({
        contactName:name
      })
    }


    splitDate = (date) => {
        let dateSplitted = date.split('-');
        return `${dateSplitted[0]}.${dateSplitted[1]}.${dateSplitted[2]}`;
    }

    getMonthAndYear = (element) => {
        const monthes = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
        let date = element.date.split('.');
        date[1] = date[1].startsWith('0') ? date[1].substring(1) : date[1];
        return `${monthes[date[1]-1]} ${date[0]}`;
    }

    groupDebts = (data, username) => {
      if (data.status !== 403) {

        data.forEach(element => {
            element.date = this.splitDate(element.date);
            element.deadLine = this.splitDate(element.deadLine);
            element.loan = (element.loaner.username === username) ? true : false;
            // console.log('loooan', element.loaner.username, username)
        });


        let tempDebtsAndLoans = this.state.debtsAndLoans;

        for (const element of data) {
          let monthAndYear = this.getMonthAndYear(element);
        //   console.log(monthAndYear);
          if (!tempDebtsAndLoans.hasOwnProperty(monthAndYear)) {
            tempDebtsAndLoans[monthAndYear] = [];
          }
            tempDebtsAndLoans[monthAndYear].push(element);
        }

        this.setState({
            debtsAndLoans: tempDebtsAndLoans
        })
    }
  }



    renderUsers = () => {
      return (
            <div id="body">
                <HeaderC title="Users"/>


                {
                  Object.entries(this.state.usersAndDebts).map((item,i) => (
                    <div key = {i} onClick = {() => this.triggerUserClick(item[0])}>
                      <User n={i+1} name = {item[0]} total = {item[1]} key = {i}></User>
                    </div>
                  ))
                }
                <BottomBar pageNumber="3"/>
            </div>
      )
    }


    renderDebts = () => {
      return (
        <div id="body">
            <HeaderC title={this.state.contactName}/>


            {

                //debtsandloans: { "yearMonth" : [{},{}], ...  }

                Object.keys(this.state.debtsAndLoans)
                    .filter(elem => this.state.debtsAndLoans[elem].length !== 0)
                    .map( item => (
                        <div className="debtContainer" key = {item}>
                            <div key = {item}>
                                <div className="debtContainerName">{item}</div>
                                {this.state.debtsAndLoans[item].map( element => (
                                    <Debt
                                    key = {element.id}
                                    setPopUpData = {this.setPopUpData}
                                    openPopUp = {this.openPopUp}
                                    id = {element.id}
                                    orientation = {element.loan ? 'left' : 'right'}
                                    name = {element.loan ? element.debtor.name : element.loaner.name}
                                    amount = {element.amount}
                                    expire = {element.deadLine}
                                    last = "n"
                                    detail = {element.desc}
                                    date = {element.date.substring(5)}
                                    location = ""
                                    tags = ""
                                />
                                ))}
                            </div>
                        </div>
                    ))
            }
            <BottomBar pageNumber="3"/>
        </div>
      )
    }

    render() {
        return this.state.showUsers ? this.renderUsers() : this.renderDebts();
    }
}


export default Users;
