import React from "react";
import HeaderC from './HeaderC';
import Debt from './Debt';
import BottomBar from "./BottomBar";
import './styles/Static.css';
import './styles/Debt.css';

class MainStatic extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            debtsAndLoans: {},
            total: -3000,
            comments: [],
            tags:[]
        };
        this.modal = React.createRef();
        this.popupContent = React.createRef();
        this.popupMessage = React.createRef();
    }


    openPopUp = () => {
        this.modal.current.style.display = "block";
    }

    closePopUp = () => {
        this.modal.current.style.display = "none";
    }

    openMessage = () => {
        this.getComments();
        this.popupContent.current.style.display = "none";
        this.popupMessage.current.style.display = "block";
    }


    getComments = () => {
        let token = JSON.parse(localStorage.getItem('token'));

        fetch(`/inDebt/comments/${this.state.id}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        .then((response) => response.json())
        .then((data) => {

            data.forEach((element) => {
                element.date = element.date.substring(0,10);
            })

            data.reverse();

            this.setState({
                comments: data
            });

            console.log('comments', data);
        });
    }


    closeMessage = () => {
        this.popupContent.current.style.display = "block";
        this.popupMessage.current.style.display = "none";
    }

    setPopUpData = (date, name, orientation, amount, detail, expire, location, id,tags) => {
        this.setState({
            date: date,
            name: name,
            orientation: orientation,
            amount: amount,
            detail: detail,
            expire: expire,
            location: location,
            id:id,
            tags:tags
        });


            console.log('state', this.state);

    }




    targetHasClickHandler(event) {
        let el = event.target;
        while (el) {
            if (el.getAttribute('data-click-handler')) {
                return true;
            }
            el = el.parentElement;
        }
        return false;
    }

    groupDebts = (data, username) => {
        console.log(data)
      if (data.status !== 403) {

        data.forEach(element => {
            element.date = this.splitDate(element.date);
            element.deadLine = this.splitDate(element.deadLine);
            element.loan = (element.loaner.username === username) ? true : false;
            // console.log('loooan', element.loaner.username, username)
        });


        let tempDebtsAndLoans = this.state.debtsAndLoans;

        for (const element of data) {
          let monthAndYear = this.getMonthAndYear(element);
        //   console.log(monthAndYear);
          if (!tempDebtsAndLoans.hasOwnProperty(monthAndYear)) {
            tempDebtsAndLoans[monthAndYear] = [];
          }
            tempDebtsAndLoans[monthAndYear].push(element);
        }

        this.setState({
            debtsAndLoans: tempDebtsAndLoans
        })
    }
  }

    //TO DO: distribute the debts/loans by username
    componentDidMount() {
        let token = JSON.parse(localStorage.getItem('token'));
        let username = JSON.parse(localStorage.getItem('username'));
        fetch('/inDebt/debts/current?filters=all,ASC', {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        .then((response) => response.json())
        .then((data) => this.groupDebts(data,username));
    }


    getMonthAndYear = (element) => {
        const monthes = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
        let date = element.date.split('.');
        date[1] = date[1].startsWith('0') ? date[1].substring(1) : date[1];
        return `${monthes[date[1]-1]} ${date[0]}`;
    }


    splitDate = (date) => {
        let dateSplitted = date.split('-');
        return `${dateSplitted[0]}.${dateSplitted[1]}.${dateSplitted[2]}`;
    }

    createUrl = () => {
      let sort = (document.querySelector("#down").checked) ? "ASC" : "DESC";

      let filter;
      for (let el of document.querySelectorAll(".radioFilter")) {
        if (el.checked) filter = el.id;
      }

      //console.log(url);
      return `/inDebt/debts/current?filters=${filter},${sort}`;
    }

    retrieveDebts = () => {
      this.setState({
          debtsAndLoans: {}
      })

      let token = JSON.parse(localStorage.getItem('token'));
      let username = JSON.parse(localStorage.getItem('username'));
      fetch(this.createUrl(), {
          method: 'GET',
          headers: {
              'Authorization': `Bearer ${token}`
          }
      })
      .then((response) => response.json())
      .then((data) => this.groupDebts(data, username));
    }

    sendComment = (key) => {
        let commentDto = {
            debtId: key,
            username: JSON.parse(localStorage.getItem('username')),
            text: document.getElementById('newMessage').value
        }

        document.getElementById('newMessage').value="";

        let token = JSON.parse(localStorage.getItem('token'));

        fetch("/inDebt/comments", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(commentDto)
        })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
        })

        setTimeout(() => {
            this.getComments();
        }, 1000*0.1);
    }


    render() {
        return (
            <div id="body">
                <HeaderC title="InDebt"/>

                <input className="radioBtns" type="radio" id="down" name="sort" value="down" defaultChecked onChange={this.retrieveDebts}/>
                <label htmlFor="down" id="downSortLabel" className="sortTriangle" >
                    <svg id="downTriangle" className="sortTriangle" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 384 384" >

                        <g>
                            <g>
                                <path d="M0,21.333l192,341.333L384,21.333H0z M72,64h240L192,277.333L72,64z"/>
                            </g>
                        </g>
                    </svg>
                </label>
                <input className="radioBtns" type="radio" id="up" name="sort" value="up" onChange={this.retrieveDebts}/>
                <label htmlFor="up" id="downSortLabel" className="sortTriangle">
                    <svg id="upTriangle" className="sortTriangle" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 384 384" >
                        <g>
                            <g>
                                <path d="M0,21.333l192,341.333L384,21.333H0z M72,64h240L192,277.333L72,64z"/>
                            </g>
                        </g>
                    </svg>
                </label>


                <input className="radioBtns radioFilter"  type="radio" id="all" name="filter" value="rg" defaultChecked onChange={this.retrieveDebts}/>
                <label htmlFor="all" id="rFilterLabel">
                    <div id="rgCircle" className="labelCircle"></div>
                    <div id="rgCircle2"></div>
                </label>
                <input className="radioBtns radioFilter" type="radio" id="loans" name="filter" value="g" onChange={this.retrieveDebts}/>
                <label htmlFor="loans" id="gFilterLabel">
                    <div id="gCircle" className="labelCircle"></div>
                </label>
                <input className="radioBtns radioFilter" type="radio" id="debts" name="filter" value="r" onChange={this.retrieveDebts}/>
                <label htmlFor="debts" id="rgFilterLabel">
                    <div id="rCircle" className="labelCircle"></div>
                </label>

                <div id="totalContainer">
                    Total: <div id="totalAmountContainer"
                                style={this.state.total < 0 ? textColorRed : normal}>{this.state.total}</div>

                </div>


                {

                    //debtsandloans: { "yearMonth" : [{},{}], ...  }

                    Object.keys(this.state.debtsAndLoans)
                        .filter(elem => this.state.debtsAndLoans[elem].length !== 0)
                        .map( item => (
                            <div className="debtContainer" key = {item}>
                                <div key = {item}>
                                    <div className="debtContainerName">{item}</div>
                                    {this.state.debtsAndLoans[item].map( element => (
                                        <Debt
                                        key = {element.id}
                                        setPopUpData = {this.setPopUpData}
                                        openPopUp = {this.openPopUp}
                                        id = {element.id}
                                        orientation = {element.loan ? 'left' : 'right'}
                                        name = {element.loan ? element.debtor.name : element.loaner.name}
                                        amount = {element.amount}
                                        expire = {element.deadLine}
                                        last = "n"
                                        detail = {element.desc}
                                        date = {element.date.substring(5)}
                                        location = ""
                                        tags = {element.tags}
                                    />
                                    ))}
                                </div>
                            </div>
                        ))
                }


                <div id="popupCont" ref={this.modal}>
                    <div id="bc" onClick={(evt) => {
                        if (this.targetHasClickHandler(evt)) {
                            return null;
                        }
                        this.closePopUp()
                    }
                    }>
                        <div id="popup" data-click-handler="true">


                            <div id="popupContent" ref={this.popupContent}>

                                <div className="debtDate">
                                    {this.state.date}
                                </div>
                                <div className="tagContainer">
                                    {
                                        this.state.tags.map(tag => (
                                            <div className="tag">{tag}</div>
                                        ))
                                    }
                                </div>
                                <svg id="debtOrientationIcon" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 480 480"
                                     style={this.state.orientation === "right" ? refilled : normal}>
                                    <g>
                                        <g>
                                            <path d="M461.248,194.736l-128-128c-24.928-24.96-65.568-24.96-90.496,0C230.656,78.8,224,94.896,224,111.984
                                    s6.656,33.184,18.752,45.248l82.752,82.752l-82.752,82.752C230.656,334.832,224,350.896,224,367.984s6.656,33.152,18.752,45.248
                                    c12.096,12.096,28.16,18.752,45.248,18.752s33.152-6.656,45.248-18.752l128-128C473.344,273.168,480,257.072,480,239.984
                                    S473.344,206.8,461.248,194.736z M438.624,262.608l-128,128c-12.128,12.096-33.12,12.096-45.248,0
                                    c-12.48-12.48-12.48-32.768,0-45.248l105.376-105.376L265.376,134.608c-6.048-6.048-9.376-14.08-9.376-22.624
                                    s3.328-16.576,9.376-22.624c6.24-6.24,14.432-9.376,22.624-9.376c8.192,0,16.384,3.136,22.624,9.344l128,128
                                    c6.048,6.08,9.376,14.112,9.376,22.656S444.672,256.56,438.624,262.608z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M237.248,194.736l-128-128c-24.928-24.96-65.568-24.96-90.496,0C6.656,78.8,0,94.896,0,111.984
                                    s6.656,33.184,18.752,45.248l82.752,82.752l-82.752,82.752C6.656,334.832,0,350.896,0,367.984s6.656,33.152,18.752,45.248
                                    c12.096,12.096,28.16,18.752,45.248,18.752s33.152-6.656,45.248-18.752l128-128C249.344,273.168,256,257.072,256,239.984
                                    S249.344,206.8,237.248,194.736z M214.624,262.608l-128,128c-12.128,12.096-33.12,12.096-45.248,0
                                    c-12.48-12.48-12.48-32.768,0-45.248l105.376-105.376L41.376,134.608C35.328,128.56,32,120.528,32,111.984
                                    s3.328-16.576,9.376-22.624c6.24-6.24,14.432-9.376,22.624-9.376s16.384,3.136,22.624,9.344l128,128
                                    c6.048,6.08,9.376,14.112,9.376,22.656S220.672,256.56,214.624,262.608z"/>
                                        </g>
                                    </g>
                                </svg>

                                <div className="nameContainer"
                                     style={this.state.orientation === "right" ? nameContRight : normal}>
                                    {this.state.name}
                                </div>
                                <div className="youContainer"
                                     style={this.state.orientation === "right" ? youContRight : normal}>
                                    YOU
                                </div>
                                <div className="amountContainer">
                                    {this.state.amount} kč
                                </div>
                                <div className="detail">
                                    {this.state.detail}
                                </div>
                                <div className="debtExpire">
                                    expire in: {this.state.expire}
                                </div>
                                <div className="debtLocation">
                                    {this.state.location}
                                </div>

                                <div id="debtEdit">
                                    <svg className="debtInfoButton" viewBox="0 0 24 24" width="512"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="m11.894 24c-.131 0-.259-.052-.354-.146-.118-.118-.17-.288-.137-.451l.707-3.535c.02-.098.066-.187.137-.256l7.778-7.778c.584-.584 1.537-.584 2.121 0l1.414 1.414c.585.585.585 1.536 0 2.121l-7.778 7.778c-.069.07-.158.117-.256.137l-3.535.707c-.032.006-.065.009-.097.009zm1.168-3.789-.53 2.651 2.651-.53 7.671-7.671c.195-.195.195-.512 0-.707l-1.414-1.414c-.195-.195-.512-.195-.707 0zm2.367 2.582h.01z"/>
                                        <path
                                            d="m9.5 21h-7c-1.379 0-2.5-1.121-2.5-2.5v-13c0-1.379 1.121-2.5 2.5-2.5h2c.276 0 .5.224.5.5s-.224.5-.5.5h-2c-.827 0-1.5.673-1.5 1.5v13c0 .827.673 1.5 1.5 1.5h7c.276 0 .5.224.5.5s-.224.5-.5.5z"/>
                                        <path
                                            d="m16.5 12c-.276 0-.5-.224-.5-.5v-6c0-.827-.673-1.5-1.5-1.5h-2c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h2c1.379 0 2.5 1.121 2.5 2.5v6c0 .276-.224.5-.5.5z"/>
                                        <path
                                            d="m11.5 6h-6c-.827 0-1.5-.673-1.5-1.5v-2c0-.276.224-.5.5-.5h1.55c.232-1.14 1.243-2 2.45-2s2.218.86 2.45 2h1.55c.276 0 .5.224.5.5v2c0 .827-.673 1.5-1.5 1.5zm-6.5-3v1.5c0 .275.225.5.5.5h6c.275 0 .5-.225.5-.5v-1.5h-1.5c-.276 0-.5-.224-.5-.5 0-.827-.673-1.5-1.5-1.5s-1.5.673-1.5 1.5c0 .276-.224.5-.5.5z"/>
                                        <path
                                            d="m13.5 9h-10c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h10c.276 0 .5.224.5.5s-.224.5-.5.5z"/>
                                        <path
                                            d="m13.5 12h-10c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h10c.276 0 .5.224.5.5s-.224.5-.5.5z"/>
                                        <path
                                            d="m13.5 15h-10c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h10c.276 0 .5.224.5.5s-.224.5-.5.5z"/>
                                    </svg>
                                </div>
                                <div id="debtDelete">
                                    <svg className="debtInfoButton" viewBox="-40 0 427 427.00131"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="m232.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0"/>
                                        <path
                                            d="m114.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0"/>
                                        <path
                                            d="m28.398438 127.121094v246.378906c0 14.5625 5.339843 28.238281 14.667968 38.050781 9.285156 9.839844 22.207032 15.425781 35.730469 15.449219h189.203125c13.527344-.023438 26.449219-5.609375 35.730469-15.449219 9.328125-9.8125 14.667969-23.488281 14.667969-38.050781v-246.378906c18.542968-4.921875 30.558593-22.835938 28.078124-41.863282-2.484374-19.023437-18.691406-33.253906-37.878906-33.257812h-51.199218v-12.5c.058593-10.511719-4.097657-20.605469-11.539063-28.03125-7.441406-7.421875-17.550781-11.5546875-28.0625-11.46875h-88.796875c-10.511719-.0859375-20.621094 4.046875-28.0625 11.46875-7.441406 7.425781-11.597656 17.519531-11.539062 28.03125v12.5h-51.199219c-19.1875.003906-35.394531 14.234375-37.878907 33.257812-2.480468 19.027344 9.535157 36.941407 28.078126 41.863282zm239.601562 279.878906h-189.203125c-17.097656 0-30.398437-14.6875-30.398437-33.5v-245.5h250v245.5c0 18.8125-13.300782 33.5-30.398438 33.5zm-158.601562-367.5c-.066407-5.207031 1.980468-10.21875 5.675781-13.894531 3.691406-3.675781 8.714843-5.695313 13.925781-5.605469h88.796875c5.210937-.089844 10.234375 1.929688 13.925781 5.605469 3.695313 3.671875 5.742188 8.6875 5.675782 13.894531v12.5h-128zm-71.199219 32.5h270.398437c9.941406 0 18 8.058594 18 18s-8.058594 18-18 18h-270.398437c-9.941407 0-18-8.058594-18-18s8.058593-18 18-18zm0 0"/>
                                        <path
                                            d="m173.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0"/>
                                    </svg>
                                </div>
                                <div id="debtComment" onClick={this.openMessage}>
                                    <svg className="debtInfoButton" viewBox="-21 -47 682.66669 682"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="m552.011719-1.332031h-464.023438c-48.515625 0-87.988281 39.464843-87.988281 87.988281v283.972656c0 48.414063 39.300781 87.816406 87.675781 87.988282v128.863281l185.191407-128.863281h279.144531c48.515625 0 87.988281-39.472657 87.988281-87.988282v-283.972656c0-48.523438-39.472656-87.988281-87.988281-87.988281zm50.488281 371.960937c0 27.835938-22.648438 50.488282-50.488281 50.488282h-290.910157l-135.925781 94.585937v-94.585937h-37.1875c-27.839843 0-50.488281-22.652344-50.488281-50.488282v-283.972656c0-27.84375 22.648438-50.488281 50.488281-50.488281h464.023438c27.839843 0 50.488281 22.644531 50.488281 50.488281zm0 0"/>
                                        <path d="m171.292969 131.171875h297.414062v37.5h-297.414062zm0 0"/>
                                        <path d="m171.292969 211.171875h297.414062v37.5h-297.414062zm0 0"/>
                                        <path d="m171.292969 291.171875h297.414062v37.5h-297.414062zm0 0"/>
                                    </svg>
                                </div>
                            </div>
                            <div id="popupMessage" ref={this.popupMessage}>
                                <div className="closePopupNested" onClick={this.closeMessage}>
                                    <svg id="closeIcon" xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 492 492">
                                        <g>
                                            <g>
                                                <path d="M300.188,246L484.14,62.04c5.06-5.064,7.852-11.82,7.86-19.024c0-7.208-2.792-13.972-7.86-19.028L468.02,7.872
                                                    c-5.068-5.076-11.824-7.856-19.036-7.856c-7.2,0-13.956,2.78-19.024,7.856L246.008,191.82L62.048,7.872
                                                    c-5.06-5.076-11.82-7.856-19.028-7.856c-7.2,0-13.96,2.78-19.02,7.856L7.872,23.988c-10.496,10.496-10.496,27.568,0,38.052
                                                    L191.828,246L7.872,429.952c-5.064,5.072-7.852,11.828-7.852,19.032c0,7.204,2.788,13.96,7.852,19.028l16.124,16.116
                                                    c5.06,5.072,11.824,7.856,19.02,7.856c7.208,0,13.968-2.784,19.028-7.856l183.96-183.952l183.952,183.952
                                                    c5.068,5.072,11.824,7.856,19.024,7.856h0.008c7.204,0,13.96-2.784,19.028-7.856l16.12-16.116
                                                    c5.06-5.064,7.852-11.824,7.852-19.028c0-7.204-2.792-13.96-7.852-19.028L300.188,246z"/>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <form>
                                    <input type="text" id="newMessage" placeholder="Type message.."
                                           className="newMessageInput"/>
                                    <button type="button" id="send" onClick={() => (this.sendComment(this.state.id))}>
                                        <svg id="sendIcon" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 448.011 448.011">
                                            <g>
                                                <g>
                                                    <path d="M438.731,209.463l-416-192c-6.624-3.008-14.528-1.216-19.136,4.48c-4.64,5.696-4.8,13.792-0.384,19.648l136.8,182.4
                                                        l-136.8,182.4c-4.416,5.856-4.256,13.984,0.352,19.648c3.104,3.872,7.744,5.952,12.448,5.952c2.272,0,4.544-0.48,6.688-1.472
                                                        l416-192c5.696-2.624,9.312-8.288,9.312-14.528S444.395,212.087,438.731,209.463z"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </button>

                                </form>


                                <div id="messagesContainer">
                                    {
                                        this.state.comments.map( comment => (

                                            <div className="message" key = {comment.id}>
                                                <div className="messageSender">{comment.user.name}</div>
                                                <div className="messageDate">{comment.date}</div>
                                                <div className="messageText">{comment.text}</div>
                                            </div>
                                        ))
                                    }


                                    {/* <div className="message">
                                        <div className="messageSender">Oly</div>
                                        <div className="messageDate">20.05.2020</div>
                                        <div className="messageText">Priv</div>
                                    </div>
                                    <div className="message">
                                        <div className="messageSender">Oly</div>
                                        <div className="messageDate">20.05.2020</div>
                                        <div className="messageText">Priv</div>
                                    </div>
                                    <div className="message">
                                        <div className="messageSender">Oly</div>
                                        <div className="messageDate">20.05.2020</div>
                                        <div className="messageText">Priv</div>
                                    </div>
                                    <div className="message">
                                        <div className="messageSender">Pasha</div>
                                        <div className="messageDate">20.05.2020</div>
                                        <div className="messageText">PrivPriv Priv Priv Priv Priv Priv Priv Priv Priv
                                            Priv Priv
                                        </div>
                                    </div>
                                    <div className="message">
                                        <div className="messageSender">Pasha</div>
                                        <div className="messageDate">20.05.2020</div>
                                        <div className="messageText">PrivPriv Priv Priv Priv Priv Priv Priv Priv Priv
                                            Priv Priv
                                        </div>
                                    </div> */}
                                </div>
                            </div>


                        </div>

                    </div>
                </div>

                <div className="placeHolder">
                </div>
                <BottomBar pageNumber="1"/>

            </div>
        );
    }


}

const showBorder = {
    border: '2px solid #333333',
};


const normal = {};
const nameContRight = {
    left: '50%',
};
const youContRight = {
    left: '0',
}
const marginTop = {
    marginTop: '15%',
};

const refilledToBlue = {
    fill: '#3E5C76'
}


const refilled = {
    fill: '#C1292E',
}
const textColorRed = {
    color: '#C1292E',
}
const lastDebt = {
    borderBottomLeftRadius: '5px',
    borderBottomRightRadius: '5px',
}

export default MainStatic;
