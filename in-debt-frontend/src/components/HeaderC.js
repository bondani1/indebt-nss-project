import React, {Component} from "react";
import './styles/Header.css'

class HeaderC extends Component {
    render() {
        return (
            <div id="header">
                <p>{this.props.title}</p>
            </div>
        );
    }
}

export default HeaderC;