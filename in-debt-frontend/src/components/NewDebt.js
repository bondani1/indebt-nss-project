import React from "react";
import './styles/NewDebt.css';
import HeaderC from './HeaderC';
import BottomBar from "./BottomBar";
import {v4 as uuidv4} from 'uuid';
import './styles/App.css';

class NewDebt extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            items: [],
            tagItems: [],
            placeItems: [],
            showingTags: [],
            showParticipantItems: false,
            showTagItems: false,
            showPlaceItems: false,
            showUser: false,
            showUserInput: true,
            showTag: false,
            showPlace: false,
            showPlaceInput: true,
            selectedItem: '',
            inputDebtName: '',
            inputParticipantValue: '',
            inputParticipantValueDisplaying: '',
            inputTagValueDisplaying: '',
            inputLocationValueDisplaying: '',
            inputAmount: '',
            inputDeadline: '',
            inputDescription: '',
            inputLocation: '',
            inputTag:'',
            isDebt: true,
            keyPressed: ''
        }
        this.handleInputUserChange = this.handleInputUserChange.bind(this);
        this.triggerKeyDown = this.triggerKeyDown.bind(this);
        this.handleOnBlur = this.handleOnBlur.bind(this);
        this.handleItemSelection = this.handleItemSelection.bind(this);
        this.handleDebtNameChange = this.handleDebtNameChange.bind(this);
        this.handleDebtCheckboxChange = this.handleDebtCheckboxChange.bind(this);
        this.handleAmountChange = this.handleAmountChange.bind(this);
        this.handleDeadlineChange = this.handleDeadlineChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleInputTagChange = this.handleInputTagChange.bind(this);
        this.createDebt = this.createDebt.bind(this);
        this.triggerCancelButton = this.triggerCancelButton.bind(this);
        this.handlePlaceSelection = this.handlePlaceSelection.bind(this);
    }


    handleInputUserChange = (event) => {
        this.setState({
            inputParticipantValue: event.target.value
        });

        //need some non-visible delay to find out which key was pressed
        setTimeout(() => {

            //TO DO: Integrate later when login, registration are fixed (localStorage API)
            let token = JSON.parse(localStorage.getItem('token'));

            if(this.state.inputParticipantValue.length >= 3) {
                this.setState({
                    showParticipantItems: true
                })

                fetch(`inDebt/users/${this.state.inputParticipantValue}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                })
                .then((response) => response.json())
                .then((data) => {
                    //TO DO: Improve to universal condition (if server response error)
                    if(data.status !== 403) {
                        if(data) {

                            let tempArray = [];

                            data.forEach(user => {
                                let newUser = {
                                    id: uuidv4(),
                                    name: user
                                }
                                tempArray.push(newUser);
                            })

                            this.setState({
                                items: tempArray
                            })
                        }
                    }
                });
            } else {
                this.setState({
                    showParticipantItems: false
                })
            }
        },0);
    }


    triggerKeyDown = (event) => {
        console.log(event.key);
        this.setState({
            keyPressed: event.key
        })
    }


    handleOnBlur = (event) => {
        setTimeout(() => {
            this.setState({
                showParticipantItems: false
            })
            console.log(this.state.showParticipantItems);
        }, 1000*0.2);
    }

    handleOnBlurForPlace = (event) => {
        setTimeout(() => {
            this.setState({
                showPlaceItems: false
            })
            console.log(this.state.showPlaceItems);
        }, 1000*0.2);
    }


    handleOnBlurForTags = (event) => {

        console.log('nekita huy', this.state.inputTag);
        let tempArray = this.state.showingTags;

        let newTag = {
            id: uuidv4(),
            name: this.state.inputTag
        }

        tempArray.push(newTag);

        this.setState({
            showingTags: tempArray,
            showTag: true,
            inputTag: ''
        });

        setTimeout(() => {
            this.setState({
                showTagItems: false
            })
            console.log(this.state.showParticipantItems);
        }, 1000*0.2);
    }


    handleItemSelection = (event) => {
        this.setState({
            inputParticipantValueDisplaying: event.target.innerHTML,
            inputParticipantValue: '',
            showUser: true,
            showUserInput: false
        })
    }


    handlePlaceSelection = (event) => {
        this.setState({
            inputLocationValueDisplaying: event.target.innerHTML,
            inputLocation: '',
            showPlace: true,
            showPlaceInput: false,
            showPlaceItems:false
        })
        console.log(this.state.showPlaceInput)
    }


    handleTagSelection = (event) => {

        //add uuidv4 for cancelling

        let newTag = {
            id: uuidv4(),
            name: event.target.innerHTML
        }


        let array = this.state.showingTags;
        array.pop();
        array.push(newTag);

        this.setState({
            showingTags: array,
            inputTagValueDisplaying: event.target.innerHTML,
            inputTag: '',
            showTag: true
        })
    }





    handleDebtNameChange = (event) => {
        this.setState({
            inputDebtName: event.target.value
        });
    }


    handleDebtCheckboxChange = (event) => {
        this.setState({
            isDebt: !this.state.isDebt
        });

        setTimeout(() => {
            console.log('here', this.state.isDebt)
        }, 0);
    }


    handleAmountChange = (event) => {
        this.setState({
            inputAmount: event.target.value
        });
    }


    handleDeadlineChange = (event) => {
        this.setState({
            inputDeadline: event.target.value
        });
    }

    handleDescriptionChange = (event) => {
        this.setState({
            inputDescription: event.target.value
        });
    }


    handleLocationChange = (event) => {
        this.setState({
            inputLocation: event.target.value
        });

        setTimeout(() => {
            let token = JSON.parse(localStorage.getItem('token'));

            if (this.state.inputLocation.length >= 3) {
                this.setState({
                    showPlaceItems: true
                });

                fetch(`inDebt/places/${this.state.inputLocation}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                })
                .then((response) => response.json())
                .then((data) => {
                    //TO DO: Improve to universal condition (if server response error)
                    if(data.status !== 403) {
                        if(data) {

                            let tempArray = [];

                            console.log('place data', data);

                            data.forEach(place => {
                                let newPlace = {
                                    id: uuidv4(),
                                    name: place
                                }
                                tempArray.push(newPlace);
                            })

                            this.setState({
                                placeItems: tempArray
                            });



                            console.log(this.state.placeItems)
                        }
                    }
                });
            } else {
                this.setState({
                    showPlaceItems: false
                });
            }

        }, 0);

    }

    handleInputTagChange = (event) => {
        this.setState({
            inputTag: event.target.value
        });


        setTimeout(() => {
            let token = JSON.parse(localStorage.getItem('token'));

            if (this.state.inputTag.length >= 1) {
                this.setState({
                    showTagItems: true
                });

                fetch(`inDebt/tags/${this.state.inputTag}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                })
                .then((response) => response.json())
                .then((data) => {
                    //TO DO: Improve to universal condition (if server response error)
                    if(data.status !== 403) {
                        if(data) {

                            let tempArray = [];


                            console.log('taags data', data);

                            data.forEach(tag => {
                                let newTag = {
                                    id: uuidv4(),
                                    name: tag
                                }
                                tempArray.push(newTag);
                            })

                            this.setState({
                                tagItems: tempArray
                            });

                            // console.log(this.state.tagItems)
                        }
                    }
                });
            } else {
                this.setState({
                    showTagItems: false
                });
            }

        }, 0);


    }

    triggerCancelButton = (event) => {
        this.setState({
            showUser:false,
            showUserInput: true
        })
    }

    triggerCancelButtonForTags = (tagId) => {
        let tempArray = this.state.showingTags
        let tag = tempArray.find(tag => tag.id === tagId);
        let index = tempArray.indexOf(tag);
        tempArray.splice(index, 1);

        //find by id -> remove
        this.setState({
            showingTags: tempArray
        })
    }

    createDebt = () => {

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = `${yyyy}-${mm}-${dd}`;

        let debtDto = {
            name: this.state.inputDebtName,
            descriptopn: this.state.inputDescription,
            deadLine: this.state.inputDeadline,
            date: today,
            participant: this.state.inputParticipantValueDisplaying,
            amount: this.state.inputAmount,
            debt: this.state.isDebt,
            placeName: this.state.inputLocationValueDisplaying,
            tags: this.state.showingTags.map(obj => obj.name)
        }

console.log(this.state.showingTags)

        let token = JSON.parse(localStorage.getItem('token'));

        fetch('/inDebt/debts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(debtDto)
          })
          .then((response) => response.json())
          .then((data) => {
            console.log('data', data);
          });

    }


    render() {
        return (
            <div id="body">
                <HeaderC title="New debt"/>
                <form>
                    <label htmlFor="debtOrientation" id="debtOrientationLabel">
                        <div id="iBorrow" style={this.state.isDebt ? boldFont : normal}>I borrow</div>
                        <div id="debtOrientationAnim">
                            <div id="debtOrientationAnimCircle" style={!this.state.isDebt ? moveToRight : normal}></div>
                        </div>
                        <div id="iLend" style={!this.state.isDebt ? boldFont : normal}>I lend</div>

                    </label>
                    <input type="checkbox" id="debtOrientation" className="formField"
                           onChange={this.handleDebtCheckboxChange}/>


                    <div className = "input-participant-container">

                            <div className = "inputParticipant" style = {{display: this.state.showUser ? 'flex' : 'none'}}>
                                <div>{this.state.inputParticipantValueDisplaying}</div>
                                <span>&nbsp;&nbsp;</span>
                                <div className = "cancel-container" onClick = {this.triggerCancelButton}>x</div>

                            </div>

                            <input
                                style = {{display: this.state.showUserInput ? 'block' : 'none'}}
                                type="text"
                                id="debtChel"
                                placeholder={!this.state.isDebt ? "Creditor" : "Debtor"}
                                value={this.state.inputParticipantValue}
                                className="input-field-participant"
                                onBlur={this.handleOnBlur}
                                onKeyDown={this.triggerKeyDown}
                                onChange={this.handleInputUserChange}
                            />

                    </div>
                    <div
                        style={{display: this.state.showParticipantItems ? 'block' : 'none'}}
                        className = "items-container"
                    >
                        {
                            this.state.items.map( item => (
                                <div
                                    key = {item.id}
                                    className = "dropdown-item"
                                    onClick = {this.handleItemSelection}
                                >
                                    { item.name }
                                </div>
                            ))
                        }
                    </div>

                    <input type="text" id="famount" placeholder="Amount" className="formField"
                        value={this.state.inputAmount} onChange={this.handleAmountChange}/>
                    <input type="date" id="deadline" className="formField" value={this.state.inputDeadline}
                        onChange={this.handleDeadlineChange}/>
                    <input type="text" id="detail" placeholder="Debt detail" className="formField"
                        value={this.state.inputDescription} onChange={this.handleDescriptionChange}/>

                    <div className = "input-place-container">

                        <div className = "inputPlace" style = {{display: this.state.showPlace ? 'flex' : 'none'}}>
                            <div>{this.state.inputLocationValueDisplaying}</div>
                            <span>&nbsp;&nbsp;</span>
                            <div className = "cancel-container" onClick = {this.triggerCancelButtonForPlace}>x</div>
                        </div>

                        <input
                            style = {{display: this.state.showPlaceInput ? 'block' : 'none'}}
                            type="text"
                            id="placech"
                            placeholder="Location"
                            value={this.state.inputLocation}
                            className="input-field-place"
                            onBlur={this.handleOnBlurForPlace}
                            onKeyDown={this.triggerKeyDown}
                            onChange={this.handleLocationChange}
                        />

                    </div>

                    <div
                    style={{display: this.state.showPlaceItems ? 'block' : 'none'}}
                    className = "place-container"
                    >
                        {
                        this.state.placeItems.map( item => (
                            <div
                                key = {item.id}
                                className = "dropdown-item"
                                onClick = {this.handlePlaceSelection}
                            >
                                { item.name }
                            </div>
                        ))
                        }
                    </div>

                    
                    


                    <div className = "input-tags-container">

                        {
                            this.state.showingTags.map(tag => (
                                <div className = "input-tag" style = {{display: this.state.showTag ? 'flex' : 'none'}}>
                                    <div>{tag.name}</div>
                                    <span>&nbsp;&nbsp;</span>
                                    <div className = "cancel-container" onClick = {() => this.triggerCancelButtonForTags(tag.id)}>x</div>
                                </div>
                            ))
                        }

                        <input 
                            type = "text" 
                            id = "tag" 
                            placeholder = "Tag" 
                            className = "input-field-tag" 
                            value = {this.state.inputTag} 
                            onBlur={this.handleOnBlurForTags}
                            onChange = {this.handleInputTagChange}
                            onKeyDown = {this.triggerKeyDown}
                        />
                    
                    </div>

                    <div
                        style={{display: this.state.showTagItems ? 'block' : 'none'}}
                        className = "tags-container"
                    >
                        {
                            this.state.tagItems.map( tag => (
                                <div
                                    key = {tag.id}
                                    className = "dropdown-item"
                                    onClick = {this.handleTagSelection}
                                >
                                    { tag.name }
                                </div>
                            ))
                        }
                    </div>

                    
                    <input type="button" id="create" value="Create" onClick={this.createDebt}/>
                </form>
                <BottomBar pageNumber="2"/>
            </div>
        );
    }
}

const normal = {}
const boldFont = {
    fontWeight: "bold",
    color: "#3E5C76"
}
const moveToRight = {
    transform: "translate(34px, 0px)",
}
export default NewDebt;
