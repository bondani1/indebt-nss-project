import React, {Component} from 'react';
import './styles/App.css';
import {Link} from 'react-router-dom';


//later remake ids to refs and maybe switch to class components
class Registration extends Component {

    constructor() {
        super();
        this.state = {
            allowed: true
        }
        this.nameInput = React.createRef();
        this.nameLabel = React.createRef();
        this.usernameInput = React.createRef();
        this.usernameLabel = React.createRef();
        this.lastnameInput = React.createRef();
        this.lastnameLabel = React.createRef();
        this.bdayInput = React.createRef();
        this.bdayLabel = React.createRef();
        this.passwordInput = React.createRef();
        this.passwordLabel = React.createRef();


    }

    register = () => {
        let registrationDto = {
            name: '',
            lastName: '',
            userName: '',
            birthDate: '',
            password: ''
        }

        registrationDto.name = document.getElementById('user-name').value;
        registrationDto.lastName = document.getElementById('user-lastname').value;
        registrationDto.userName = document.getElementById('user-username').value;
        registrationDto.birthDate = document.getElementById('user-birthDate').value;
        registrationDto.password = document.getElementById('user-password').value;

        console.log(registrationDto);

        fetch('/inDebt/register', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(registrationDto)
        })

            .then((data) => {
                console.log(data)
            });
    }

    moveNameLabel = () => {
        this.nameLabel.current.style.transform = "translate(0px, 0px)";
        this.nameLabel.current.style.fontSize = "1em";
    }
    moveNameLabelBack = () => {
        if (this.nameInput.current.value == "") {
            this.nameLabel.current.style.transform = "translate(0px,90%)";
            this.nameLabel.current.style.fontSize = "1.2em";
        }

    }
    moveLastnameLabel = () => {
        this.lastnameLabel.current.style.transform = "translate(0px, 0px)";
        this.lastnameLabel.current.style.fontSize = "1em";
    }
    moveLastnameLabelBack = () => {
        if (this.lastnameInput.current.value == "") {
            this.lastnameLabel.current.style.transform = "translate(0px,90%)";
            this.lastnameLabel.current.style.fontSize = "1.2em";
        }

    }
    moveUsernameLabel = () => {
        this.usernameLabel.current.style.transform = "translate(0px, 0px)";
        this.usernameLabel.current.style.fontSize = "1em";
    }
    moveUsernameLabelBack = () => {
        if (this.usernameInput.current.value == "") {
            this.usernameLabel.current.style.transform = "translate(0px,90%)";
            this.usernameLabel.current.style.fontSize = "1.2em";
        } else {

        fetch(`inDebt/users/${this.usernameInput.current.value}`, {
            method: 'GET'
        }).then(response => response.json())
        .then(data => {
            if (data.length != 0) {
                this.setState({
                    allowed: false
                })
            }
        })
    }

    }
    moveBdayLabel = () => {
        this.bdayLabel.current.style.transform = "translate(0px, 0px)";
        this.bdayLabel.current.style.fontSize = "1em";
    }
    moveBdayLabelBack = () => {
        if (this.bdayInput.current.value == "") {
            this.bdayLabel.current.style.transform = "translate(0px,90%)";
            this.bdayLabel.current.style.fontSize = "1.2em";
        }

    }
    movePasswordLabel = () => {
        this.passwordLabel.current.style.transform = "translate(0px, 0px)";
        this.passwordLabel.current.style.fontSize = "1em";
    }
    movePasswordLabelBack = () => {
        if (this.passwordInput.current.value == "") {
            this.passwordLabel.current.style.transform = "translate(0px,90%)";
            this.passwordLabel.current.style.fontSize = "1.2em";
        }

    }


    render() {
        return (
            <div>
                <label htmlFor="user-name" className="loginLabel" id="regLabelName" ref={this.nameLabel}>
                    Name
                </label>
                <input type="text" id="user-name" className="loginInput" ref={this.nameInput}
                       onFocus={this.moveNameLabel} onBlur={this.moveNameLabelBack}/>

                <label htmlFor="user-lastname" className="loginLabel" id="regLabelLastname" ref={this.lastnameLabel}>
                    Lastname
                </label>
                <input type="text" id="user-lastname" className="loginInput" ref={this.lastnameInput}
                       onFocus={this.moveLastnameLabel} onBlur={this.moveLastnameLabelBack}/>

                <label htmlFor="user-username" className="loginLabel" id="regLabelUsername" ref={this.usernameLabel}>
                    Username
                </label>
                <input type="text" id="user-username" className="loginInput" ref={this.usernameInput}
                       onFocus={this.moveUsernameLabel} onBlur={this.moveUsernameLabelBack}/>
                <p id="usernameError" style={{display: this.state.allowed ? "none" : "block"}}>Username already
                    exists</p>

                <label htmlFor="user-birthDate" className="loginLabel" id="regLabelBday" ref={this.bdayLabel}>
                    Birthday
                </label>
                <input type="date" id="user-birthDate" className="loginInput" ref={this.bdayInput}/>

                <label htmlFor="user-password" className="loginLabel" id="regLabelPassword" ref={this.passwordLabel}>
                    Password
                </label>
                <input type="password" id="user-password" className="loginInput" ref={this.passwordInput}
                       onFocus={this.movePasswordLabel} onBlur={this.movePasswordLabelBack}/>

                <Link to="/login">
                    <div id="regButton" onClick={this.register}>Register</div>
                </Link>

            </div>
        );
    }
}


//
// const register = () => {
//     let registrationDto = {
//         name: '',
//         lastName: '',
//         userName: '',
//         birthDate: '',
//         password: ''
//     }
//
//     registrationDto.name = document.getElementById('user-name').value;
//     registrationDto.lastName = document.getElementById('user-lastname').value;
//     registrationDto.userName = document.getElementById('user-username').value;
//     registrationDto.birthDate = document.getElementById('user-birthDate').value;
//     registrationDto.password = document.getElementById('user-password').value;
//
//     console.log(registrationDto);
//
//     fetch('/inDebt/register', {
//         method: 'POST',
//         headers: { 'Content-Type': 'application/json'},
//         body: JSON.stringify(registrationDto)
//       })
//
//       .then((data) => {
//           console.log(data)
//       });
// }
//
//
// const Login = () => {
//     return(
//         <MDBContainer>
//             <MDBRow id = "registration-form">
//                 <MDBCol md="6">
//                 <form>
//                     <p className="h5 text-center mb-4">Sign up</p>
//                     <div className="grey-text">
//                     <MDBInput id = "user-name" label="Your name" icon="user" group type="text" validate error="wrong"
//                         success="right" />
//                     <MDBInput id = "user-lastname" label="Your lastname" icon="user" group type="text" validate error="wrong"
//                         success="right" />
//                     <MDBInput id = "user-username" label="Your username" icon="user" group type="text" validate
//                         error="wrong" success="right" />
//                     <MDBInput id = "user-birthDate" label="Your birth date" icon="birthday-cake" group type="text" validate
//                         error="wrong" success="right" />
//                     <MDBInput id = "user-password" label="Your password" icon="lock" group type="password" validate />
//                     </div>
//                     <div className="text-center">
//                         <Link to = "/login">
//                             <MDBBtn color="primary" type = "button" onClick = {register}>Register</MDBBtn>
//                         </Link>
//                     </div>
//                 </form>
//                 </MDBCol>
//             </MDBRow>
//         </MDBContainer>
//
//     )
// }

export default Registration;