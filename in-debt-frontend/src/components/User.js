import React from "react";
import './styles/User.css';

class User extends React.Component {
    render() {
        return (
            <div className="user">
                <div className="innerContainer" id="userNumber">
                    {this.props.n}
                </div>
                <div className="innerContainer" id="userName">
                    {this.props.name}
                </div>
                <div className="innerContainer" id="userTotal" style={this.props.total < 0 ? textColorRed : normal}>
                    {this.props.total} kč
                </div>
            </div>
        );
    }
}

const normal = {};
const textColorRed = {
    color: '#C1292E',
}
export default User;