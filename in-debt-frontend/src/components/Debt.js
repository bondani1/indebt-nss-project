import React, {Component} from "react";
import './styles/Debt.css'


class Debt extends Component {
    state = {
        isOpen: false,
        myState: 0
    };

    constructor() {
        super();
        this.stateContainer = React.createRef();
    }

    toggle = () => {

        this.props.setPopUpData(
            this.props.date, 
            this.props.name, 
            this.props.orientation, 
            this.props.amount, 
            this.props.detail, 
            this.props.expire, 
            this.props.location, 
            this.props.id, 
            this.props.tags.map(tag => tag.name), 
            this.state.myState
            );
        this.props.openPopUp()

    }
    changeState = (newState) => {
        switch (newState) {
            case 0:
                this.state.myState = newState;
                this.stateContainer.current.innerText = "active";
                this.stateContainer.current.color = "#3F826D"
                break
            case 1:
                this.state.myState = newState;
                this.stateContainer.current.innerText = "paid";
                this.stateContainer.current.color = "#ffd815"
                break
            case 2:
                this.state.myState = newState;
                this.stateContainer.current.innerText = "expired";
                this.stateContainer.current.color = "#C1292E"
                break
        }
    }

    render() {
        return (

            <div id="debt" className="debtClass" style={this.props.last === "y" ? lastDebt : normal}
                 onClick={this.toggle}>

                <div id="imgContainer" style={this.props.orientation === "right" ? recolored : normal}>
                    <svg className="img" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 550 550 " style={this.props.orientation === "right" ? rotated : normal}>
                        <g>
                            <g>
                                <path d="M224.334,510.96c8.758,13.709,23.586,21.193,38.721,21.193c8.458,0,17.014-2.338,24.67-7.228
                                c21.358-13.647,27.619-42.032,13.966-63.391l-124.879-195.46l124.879-195.46c13.647-21.365,7.393-49.744-13.966-63.391
                                c-21.354-13.642-49.744-7.393-63.392,13.966L83.66,241.362c-9.627,15.067-9.627,34.357,0,49.425L224.334,510.96z"/>
                                <path d="M230.454,290.787L371.128,510.96c8.765,13.709,23.587,21.193,38.722,21.193c8.458,0,17.014-2.338,24.67-7.228
                                c21.358-13.647,27.613-42.032,13.966-63.391l-124.885-195.46l124.891-195.46c13.642-21.359,7.393-49.744-13.966-63.391
                                c-21.353-13.642-49.743-7.393-63.391,13.966L230.454,241.362C220.821,256.43,220.821,275.72,230.454,290.787z"/>
                            </g>
                        </g>
                    </svg>
                </div>
                <div id="nameContainer">
                    <p id="name">{this.props.name}</p>
                </div>
                <div id="amountContainer">
                    <p id="amount">{this.props.amount}kč</p>
                </div>
                <div id="expireContainer">
                    <p id="expire">{this.props.date}</p>
                </div>
                <div className="stateContainer" ref={this.stateContainer}>
                    active
                </div>

            </div>

        );
    }

}


const modalStyle = {
    content: {
        marginTop: '15vh',
        height: '55vh',
        border: '2px solid #333333',
        borderRadius: '7px',
    },

    overlay: {
        backgroundColor: 'rgba(201,201,201,0.32)',
    }
};


const normal = {};
const nameContRight = {
    left: '50%',
};
const youContRight = {
    left: '0',
}
const rotated = {
    transform: 'rotate(180deg)',
};
const recolored = {
    backgroundColor: '#C1292E',
}
const refilled = {
    fill: '#C1292E',
}
const lastDebt = {
    borderBottomLeftRadius: '5px',
    borderBottomRightRadius: '5px',
}

export default Debt;
